//
//  Constants.swift
//  UberdooX
//
//  Created by Karthik Sakthivel on 21/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Constants {
    //App Constants
    static var baseURL = "http://35.178.27.62/Aclena/public/provider"
    static var adminBaseURL = "http://35.178.27.62/Aclena/public/admin"
    static var mapsKey = "AIzaSyDcuEgb-Jr0-QKM6SdFd3WxCPhEeukUHtM"
    static var placesKey = "AIzaSyDcuEgb-Jr0-QKM6SdFd3WxCPhEeukUHtM"
    static var locations : [JSON] = []
    static var timeSlots : [JSON] = []
}
func validateEmail(enteredEmail:String) -> Bool {
    
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: enteredEmail)
    
}
