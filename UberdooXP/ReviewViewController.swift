//
//  ReviewViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 02/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Cosmos
import SwiftyJSON
import Alamofire
import SwiftSpinner

class ReviewViewController: UIViewController {
    
    @IBOutlet weak var reviewFld: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var topView: UIView!
    var hasAlreadyMoved = false
    var bookingId : String!
    var userId : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.topView.frame.origin.y = self.topView.frame.origin.y + 130
        self.bottomView.frame.origin.y = self.bottomView.frame.origin.y + 270
        ratingView.didFinishTouchingCosmos = {
            rating in
            
            if(!self.hasAlreadyMoved)
            {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.showHideTransitionViews, animations: {
                    
                    self.hasAlreadyMoved = true
                    self.topView.frame.origin.y = self.topView.frame.origin.y - 130
                    self.bottomView.frame.origin.y = self.bottomView.frame.origin.y - 270
                    
                }) { (Void) in
                    
                }
            }
            print(rating)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func sendReview(_ sender: Any) {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let rating = String(ratingView.rating)
        let feedback = reviewFld.text
        print(userId)
        print(bookingId)
        print(rating)
        print(feedback!)
        let params: Parameters = [
            "id": userId,
            "rating": rating ,
            "booking_id":bookingId,
            "feedback":feedback!
        ]
        
        
        SwiftSpinner.show("Thanks for your review.")
        let url = "\(Constants.baseURL)/userreviews"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("REVIEW JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    
                    
                    if(jsonResponse["error"].stringValue == "true")
                    {
                        let errorMessage = jsonResponse["error_message"].stringValue
                        self.showAlert(title: "Failed",msg: errorMessage)
                    }
                    else{
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
            }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}

